//katas1//

function reverseString (str) {
    return str.split("").reverse().join("");
    // return 0;
}


function testReverseString1 () {
    console.assert(reverseString("rafael") === "leafar", "falhou usando 'rafael'");
}
testReverseString1();

function testReverseString2 () {
    console.assert(reverseString("vanessa") === "assenav", "falhou usando 'vanessa'");
}
testReverseString2();


//katas2//

function reverseSentence (str) {
    return str.split(" ").reverse().join(" ");
    // return 0;
}


function testReverseSentence1 () {
    console.assert(reverseSentence("A vida é linda") === "linda é vida A", "falhou usando 'A vida é linda'");
}
testReverseSentence1();

function testReverseSentence2 () {
    console.assert(reverseSentence("Nunca desista de aprender") === "aprender de desista Nunca", "falhou usando 'Nunca desista de aprender'");
}
testReverseSentence2();


//katas3//

function minimumValue (arr) {       
    return Math.min(...arr);
    // return 0;     
}


function testMinimumValue1 () {
    console.assert(minimumValue([356,61,5398,23547]) === 61, "falhou usando [356,61,5398,23547]");
}
testMinimumValue1();

function testMinimumValue2 () {
    console.assert(minimumValue([910,11,734,44]) === 11, "falhou usando [910,11,734,44]");
}
testMinimumValue2();


//katas4//

function maximumValue (arr) {       
    return Math.max(...arr);
    // return 0;     
}


function testMaximumValue1 () {
    console.assert(maximumValue([356,61,5398,23547]) === 23547, "falhou usando [356,61,5398,23547]");
}
testMaximumValue1();

function testMaximumValue2 () {
    console.assert(maximumValue([910,11,734,44]) === 910, "falhou usando [910,11,734,44]");
}
testMaximumValue2();

//katas5//

function calculateRemainder (a,b) {
    return a % b;
    // return 0;
}


function testCalculateRemainder1() {
    console.assert(calculateRemainder(10,3) === 1, "falhou usando (10,3)");
}
testCalculateRemainder1();

function testCalculateRemainder2() {
    console.assert(calculateRemainder(50,6) === 2, "falhou usando (50,6)");
}
testCalculateRemainder2();


//katas6//


function distinctValues (arr) {
      
    let newarr = [];
    for (let i = 0; i < arr.length; i++){
        if (newarr.indexOf(arr[i]) < 0) {
            newarr.push(arr[i]);
        }
    }    
    return JSON.stringify(newarr);  
    // return 0;
}  


function testDistinctValues1() {
    console.assert(distinctValues([1,3,5,2,1,4,5]) === JSON.stringify([1,3,5,2,4]), "falhou usando [1,3,5,2,1,4,5]");
}
testDistinctValues1();

function testDistinctValues2() {
    console.assert(distinctValues([0,2,3,1,4,4,2]) === JSON.stringify([0,2,3,1,4]), "falhou usando [0,2,3,1,4,4,2]");
}
testDistinctValues2();





